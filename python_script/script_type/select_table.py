import pandas as pd
import mysql.connector

# Variable de choix MySQL
current_database = "data"
current_table = "department"

# Création de la connection
engine = mysql.connector.connect(
    host="localhost", 
    user="antony", 
    password="choupette",
    database= current_database
)

print("Etat de la connection:")
print(engine)

mycursor = engine.cursor()

# Choix de la database:
print("Database choisit: " + current_database)
mycursor.execute("USE " + current_database)


# Fonction listant les tables:
def liste_tables():
    print("Liste tables:")
    mycursor.execute("SHOW TABLES")
    for y in mycursor:
        print(y)
# Appel-Fonction pour la table:
liste_tables()
# Choix de la table:
print("Table choisit: " + current_table)