# Identification des fichiers BDD

## Liste des fichiers:
1. departement.sql
2. employees.csv
3. products_date.json
4. products.xlsx

### 1 - Table et nom de colonne
1. departement(dept_name, dept_num)
2. employees(id, first_name, last_name, email, job)
3. products_date(product_id, max_date)
4. products(id, name, price, available, description)

### 2 - Type d'erreur relevé
1. doublon de dept_name
> Services
> Product Management
> Training
> Legal
> Marketing
> Research and Development
2. A rechercher
3. A rechercher
4. A rechercher

## Méthodologie

1. Création du MCD

> Table et nom de colonne

2. Création du MLD (UML)

![schema de la bdd](MLDecorp.png)

