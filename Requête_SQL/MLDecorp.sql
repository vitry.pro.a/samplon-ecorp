CREATE TABLE `product` (
  `id` integer PRIMARY KEY,
  `name` varchar(50),
  `date` date,
  `description` text,
  `price` decimal,
  `available` boolean
);

CREATE TABLE `employee` (
  `id` integer PRIMARY KEY,
  `first_name` varchar(50),
  `last_name` varchar(50),
  `email` varchar(256),
  `job` integer COMMENT 'departement_id'
);

CREATE TABLE `departement` (
  `id` integer PRIMARY KEY,
  `name` varchar(50)
);

ALTER TABLE `employee` ADD FOREIGN KEY (`job`) REFERENCES `departement` (`id`);
