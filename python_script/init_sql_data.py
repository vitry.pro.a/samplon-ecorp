import pandas as pd
import mysql.connector

# Variable de choix MySQL
current_database = "data"
current_table = "department"

# Création de la connection
engine = mysql.connector.connect(
    host="localhost", 
    user="antony", 
    password="choupette",
    database= current_database
)

print("Etat de la connection:")
print(engine)
print()

mycursor = engine.cursor()

# Fonction listant les databases:
def liste_database():
    print("Liste database:")
    mycursor.execute("SHOW DATABASES")
    for x in mycursor:
        print(x)
# Appel-Fonction pour la database:
liste_database()
print()
# Choix de la database:
print("Database choisit: " + current_database)
mycursor.execute("USE " + current_database)
print()


# Fonction listant les tables:
def liste_tables():
    print("Liste tables:")
    mycursor.execute("SHOW TABLES")
    for y in mycursor:
        print(y)
# Appel-Fonction pour la table:
print("Liste des tables avant création:")
# Supprime la table si exist:
# mycursor.execute("DROP TABLE IF EXISTS " + current_table)
liste_tables()
print()
# Choix de la table:
print("Table choisit: " + current_table)
print("Commande non implanté: mode console uniquement")
print()
# Appel-Fonction pour la table:
print("Liste des tables après création:")
liste_tables()
# Affiche le detail de la table:
df = pd.read_sql_query("DESCRIBE department", engine)
print(df)