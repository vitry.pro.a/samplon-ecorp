### Met à jour la liste de paquet
```sh
sudo apt update
```

### Install MySQL
```sh
sudo apt install mysql-server
```

### Démarre mySQL via un fichier
```sh
sudo mysql < nom_du_fichier.sql
```

### Démarre mySQL
```sh
sudo systenctl start mysql
sudo mysql
```
### Création utilisateur
> Lance mysql avec des permissions
```sh
mysql -u antony -p
```
> Creer un nouveau utilisateur
```sql
CREATE USER 'antony'@'localhost' IDENTIFIED BY 'choupette';
GRANT ALL PRIVILEGES ON *.*  TO 'antony'@'localhost';
```

### Afficher les Databases
```sql
SHOW DATABASES;
```

### Afficher la Database
```sql
SHOW nom_de_la_database;
```

### Créer une Database
```sql
CREATE DATABASE;
CREATE DATABASE IF NOT EXISTS nom_base;
```

### Sélectionner une Database
```sql 
USE nom_de_la_database;
```

### Afficher les tables
```sql
SHOW TABLES;
```

### Afficher la table
```sql
SHOW nom_de_la_table;
```

### Creer une table
```sql
CREATE TABLE [nom_de_table] ([nom_de_champ] [type_de_données_de_champ]([taille]));
```
> exemple:
```sql
create table department (dept_name VARCHAR(50),	dept_num INT);
```

### Afficher le contenu de la table
```sql
DESCRIBE nom_de_la_table;
```
## Sortir de mysql
```sql
exit;
```

petit truc sympa pour ce connecter : sudo mysql -Decorp --prompt="(\u@\h) [\d]> "
-D indique la base ou l'on ce connecte
--prompt="(\u@\h) [\d]> " permet d'avoir apres un prompt de type : (ecouser@localhost) [ecorp]
pour injecter un script facilement : sudo mysql -Decorp < ./init_struct_database.sql

sudo mysql -D data < department.sql^C