import pandas as pd
import mysql.connector
from sqlalchemy import create_engine

# Dataframe CSV
df1 = pd.read_csv('employees.csv', index_col=False)
print("DataFrame generated using CSV file:")
print(df1.head())
print()

# Dataframe JSON
df2 = pd.read_json("products_date.json")
#print("DataFrame generated using JSON file:")
#print(df2.head())
#print()

# Dataframe EXCEL
df3 = pd.read_excel('products.xlsx')
#print("DataFrame generated using XLSX file:")
#print(df3.head())
#print()

# Dataframe SQL
# N/A

# Affichage de la colonne
#print(df1["first_name"])
print()
# Affichage des index 1 à 20 et toutes les colonnes
#print(df1.iloc[0:20, : ])
print()
# Select rows where df1.A is greater than 0
print("Sélection id >= 190")
#print(df1[df1["id"] >= 190])
print()
# Using isin() method for filtering:
#print("Extraction ligne ou first_name = janos")
#print(df1[df1["first_name"].isin(["Janos"])])
print()

# Récupération des en-tetes:
def header(data_frame):
    key = []
    for e in data_frame.columns:
        #key.append(data_frame.columns[e])
        key = data_frame[data_frame.columns[0]].tolist()
    return key


def check_doublon(data_frame, column):
    return data_frame.duplicated(column)

def nb_doublon(data_frame):
    c = 0
    for j in range(len(data_frame)):
        if i == True:
            c += 1
        print(data_frame.loc[j, 'first_name'])
    print(f'Le nombre de doublon est {c}')


#nb_doublon(df1)

print(header(df1))
