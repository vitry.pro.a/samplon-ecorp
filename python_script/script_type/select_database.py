import pandas as pd
import mysql.connector

# Variable de choix MySQL
current_database = "data"
current_table = "department"

# Création de la connection
engine = mysql.connector.connect(
    host="localhost", 
    user="antony", 
    password="choupette",
    database= current_database
)

print("Etat de la connection:")
print(engine)

mycursor = engine.cursor()

# Fonction listant les databases:
def liste_database():
    print("Liste database:")
    mycursor.execute("SHOW DATABASES")
    for x in mycursor:
        print(x)
# Appel-Fonction pour la database:
liste_database()
# Choix de la database:
print("Database choisit: " + current_database)
mycursor.execute("USE " + current_database)