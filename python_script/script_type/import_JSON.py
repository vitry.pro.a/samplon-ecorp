import pandas as pd

df_json = pd.read_json("products_date.json")

print("DataFrame generated using JSON file:")
print(df_json.head())