import pandas as pd

df_csv = pd.read_csv('employees.csv', index_col=0)

print("DataFrame generated using CSV file:")
print(df_csv.head())
print()

# Affichage éléments du df
print(df_csv.index)
print()
print(df_csv.columns)
print()
print(df_csv.to_numpy())
print()
print(df_csv.describe())
print()
print(df_csv.T)
print()
print(df_csv.sort_index(axis=1, ascending=True))
print()
print(df_csv.sort_values(by="first_name"))
print()


# Affichage de la colonne
print(df_csv["id"])
print()
# Affichage des colonnes 0 à 3
print(df_csv[0:3])
print()
# Affichage des index 1/50/100 et les colonnes 1 à 4 sauf 3
print(df_csv.iloc[[1, 50, 100], [0, 1, 2, 4]]) # iloc[idex, column] => choix exacte
print()
# Affichage des index 1 à 20 et toutes les colonnes
print(df_csv.iloc[1:20, : ]) 


# Select rows where df_csv.A is greater than 0
print("Sélection id > 190")
print(df_csv[df_csv["id"] > 190])
print()
# Using isin() method for filtering:
print("Extraction ligne ou job = 12 ou 7")
print(df_csv[df_csv["job"].isin([12, 7])])
print()
print("Extraction ligne ou first_name = janos")
print(df_csv[df_csv["first_name"].isin(["Janos"])])
print()
# A where operation with setting: (changement signe -)
df = df_csv[df_csv > 0] = -df_csv
print(df)
print()

# Opérations sur df
# Calculate the mean value for each column:
print(df.mean())
# Calculate the mean value for each row:
df.mean(axis=1)
# Concatenating pandas objects together row-wise with concat():
# break it into pieces
pieces = [df[:3], df[3:7], df[7:]]
# Concat
pd.concat(pieces)