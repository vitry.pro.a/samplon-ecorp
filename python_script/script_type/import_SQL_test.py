
# Variable de choix MySQL:
current_database = "data"
current_table = "department"

# Création de la connection:
engine = mysql.connector.connect(
    host="localhost", 
    user="antony", 
    password="choupette",
    database= current_database
)

# Création du curseur:
#mycursor = engine.cursor()

# Affiche les databases:
#mycursor.execute("SHOW DATABASES")
data = pd.read_sql_query("SHOW DATABASES", engine)
print(data)

# Choix de la database:
#mycursor.execute("USE " + current_database)
use = pd.read_sql_query("USE data", engine)
print(use)

# Affiche les tables:
#mycursor.execute("SHOW TABLES")
table = pd.read_sql_query("SHOW TABLES", engine)
print(table)

# Création du dataframe via database "data":
df4 = pd.read_sql("SELECT [dept_name], [dept_num] FROM department", engine)
print("DataFrame generated using XLSX file:")
print(df4.head())

# Close the Database connection
engine.dispose()