import pandas as pd
import mysql.connector
from sqlalchemy import create_engine

# Dataframe CSV
df1 = pd.read_csv('employees.csv', index_col=None)
print("DataFrame generated using CSV file:")
print(df1.head())
print()

# Affichage de la colonne
print(df1["id"])
print()
# Affichage des colonnes 0 à 3
print(df1[0:3])
print()
# Affichage des index 1/50/100 et les colonnes 1 à 4 sauf 3
print(df1.iloc[[1, 50, 100], [0, 1, 2, 4]]) # iloc[idex, column] => choix exacte
print()
# Affichage des index 1 à 20 et toutes les colonnes
print(df1.iloc[1:20, : ])
print()

# Select rows where df1.A is greater than 0
print("Sélection id > 190")
print(df1[df1["id"] > 190])
print()
# Using isin() method for filtering:
print("Extraction ligne ou job = 12 ou 7")
print(df1[df1["job"].isin([12, 7])])
print()
print("Extraction ligne ou first_name = janos")
print(df1[df1["first_name"].isin(["Janos"])])
print()
# A where operation with setting: (changement signe -)
df = df1[df1 > 0] = -df1
print(df)
print()

# Opérations sur df
# Calculate the mean value for each column:
print(df.mean())
# Calculate the mean value for each row:
df.mean(axis=1)
# Concatenating pandas objects together row-wise with concat():
# break it into pieces
pieces = [df[:3], df[3:7], df[7:]]
# Concat
pd.concat(pieces)
